<?php

namespace Drupal\apache_openmeetings\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Apache Openmeetings entities.
 *
 * @ingroup apache_openmeetings
 */
interface ApacheOpenmeetingsInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Apache Openmeetings name.
   *
   * @return string
   *   Name of the Apache Openmeetings.
   */
  public function getName();

  /**
   * Sets the Apache Openmeetings name.
   *
   * @param string $name
   *   The Apache Openmeetings name.
   *
   * @return \Drupal\apache_openmeetings\Entity\ApacheOpenmeetingsInterface
   *   The called Apache Openmeetings entity.
   */
  public function setName($name);

  /**
   * Gets the Apache Openmeetings creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Apache Openmeetings.
   */
  public function getCreatedTime();

  /**
   * Sets the Apache Openmeetings creation timestamp.
   *
   * @param int $timestamp
   *   The Apache Openmeetings creation timestamp.
   *
   * @return \Drupal\apache_openmeetings\Entity\ApacheOpenmeetingsInterface
   *   The called Apache Openmeetings entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Apache Openmeetings published status indicator.
   *
   * Unpublished Apache Openmeetings are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Apache Openmeetings is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Apache Openmeetings.
   *
   * @param bool $published
   *   TRUE to set this Apache Openmeetings to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\apache_openmeetings\Entity\ApacheOpenmeetingsInterface
   *   The called Apache Openmeetings entity.
   */
  public function setPublished($published);

  /**
   * Gets the Apache Openmeetings revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Apache Openmeetings revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\apache_openmeetings\Entity\ApacheOpenmeetingsInterface
   *   The called Apache Openmeetings entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Apache Openmeetings revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Apache Openmeetings revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\apache_openmeetings\Entity\ApacheOpenmeetingsInterface
   *   The called Apache Openmeetings entity.
   */
  public function setRevisionUserId($uid);

}
