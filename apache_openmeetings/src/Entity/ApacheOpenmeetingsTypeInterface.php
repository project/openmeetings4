<?php

namespace Drupal\apache_openmeetings\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Apache Openmeetings type entities.
 */
interface ApacheOpenmeetingsTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
