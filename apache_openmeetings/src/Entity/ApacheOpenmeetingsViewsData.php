<?php

namespace Drupal\apache_openmeetings\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Apache Openmeetings entities.
 */
class ApacheOpenmeetingsViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
