<?php

namespace Drupal\apache_openmeetings\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Apache Openmeetings type entity.
 *
 * @ConfigEntityType(
 *   id = "apache_openmeetings_type",
 *   label = @Translation("Apache Openmeetings type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\apache_openmeetings\ApacheOpenmeetingsTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\apache_openmeetings\Form\ApacheOpenmeetingsTypeForm",
 *       "edit" = "Drupal\apache_openmeetings\Form\ApacheOpenmeetingsTypeForm",
 *       "delete" = "Drupal\apache_openmeetings\Form\ApacheOpenmeetingsTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\apache_openmeetings\ApacheOpenmeetingsTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "apache_openmeetings_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "apache_openmeetings",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/apache_openmeetings_type/{apache_openmeetings_type}",
 *     "add-form" = "/admin/structure/apache_openmeetings_type/add",
 *     "edit-form" = "/admin/structure/apache_openmeetings_type/{apache_openmeetings_type}/edit",
 *     "delete-form" = "/admin/structure/apache_openmeetings_type/{apache_openmeetings_type}/delete",
 *     "collection" = "/admin/structure/apache_openmeetings_type"
 *   }
 * )
 */
class ApacheOpenmeetingsType extends ConfigEntityBundleBase implements ApacheOpenmeetingsTypeInterface {

  /**
   * The Apache Openmeetings type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Apache Openmeetings type label.
   *
   * @var string
   */
  protected $label;

}
