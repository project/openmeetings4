<?php

namespace Drupal\apache_openmeetings;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Apache Openmeetings entity.
 *
 * @see \Drupal\apache_openmeetings\Entity\ApacheOpenmeetings.
 */
class ApacheOpenmeetingsAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\apache_openmeetings\Entity\ApacheOpenmeetingsInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished apache openmeetings entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published apache openmeetings entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit apache openmeetings entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete apache openmeetings entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add apache openmeetings entities');
  }

}
