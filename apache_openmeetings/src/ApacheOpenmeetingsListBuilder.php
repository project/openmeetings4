<?php

namespace Drupal\apache_openmeetings;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Apache Openmeetings entities.
 *
 * @ingroup apache_openmeetings
 */
class ApacheOpenmeetingsListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Apache Openmeetings ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\apache_openmeetings\Entity\ApacheOpenmeetings */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.apache_openmeetings.edit_form',
      ['apache_openmeetings' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
