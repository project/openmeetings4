<?php

namespace Drupal\apache_openmeetings;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for apache_openmeetings.
 */
class ApacheOpenmeetingsTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
