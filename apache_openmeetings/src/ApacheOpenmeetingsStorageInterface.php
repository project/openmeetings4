<?php

namespace Drupal\apache_openmeetings;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\apache_openmeetings\Entity\ApacheOpenmeetingsInterface;

/**
 * Defines the storage handler class for Apache Openmeetings entities.
 *
 * This extends the base storage class, adding required special handling for
 * Apache Openmeetings entities.
 *
 * @ingroup apache_openmeetings
 */
interface ApacheOpenmeetingsStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Apache Openmeetings revision IDs for a specific Apache Openmeetings.
   *
   * @param \Drupal\apache_openmeetings\Entity\ApacheOpenmeetingsInterface $entity
   *   The Apache Openmeetings entity.
   *
   * @return int[]
   *   Apache Openmeetings revision IDs (in ascending order).
   */
  public function revisionIds(ApacheOpenmeetingsInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Apache Openmeetings author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Apache Openmeetings revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\apache_openmeetings\Entity\ApacheOpenmeetingsInterface $entity
   *   The Apache Openmeetings entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(ApacheOpenmeetingsInterface $entity);

  /**
   * Unsets the language for all Apache Openmeetings with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
