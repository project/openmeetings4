<?php

namespace Drupal\apache_openmeetings;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\apache_openmeetings\Entity\ApacheOpenmeetingsInterface;

/**
 * Defines the storage handler class for Apache Openmeetings entities.
 *
 * This extends the base storage class, adding required special handling for
 * Apache Openmeetings entities.
 *
 * @ingroup apache_openmeetings
 */
class ApacheOpenmeetingsStorage extends SqlContentEntityStorage implements ApacheOpenmeetingsStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(ApacheOpenmeetingsInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {apache_openmeetings_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {apache_openmeetings_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(ApacheOpenmeetingsInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {apache_openmeetings_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('apache_openmeetings_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
