<?php

namespace Drupal\apache_openmeetings\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\apache_openmeetings\Entity\ApacheOpenmeetingsInterface;

/**
 * Class ApacheOpenmeetingsController.
 *
 *  Returns responses for Apache Openmeetings routes.
 */
class ApacheOpenmeetingsController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Apache Openmeetings  revision.
   *
   * @param int $apache_openmeetings_revision
   *   The Apache Openmeetings  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($apache_openmeetings_revision) {
    $apache_openmeetings = $this->entityManager()->getStorage('apache_openmeetings')->loadRevision($apache_openmeetings_revision);
    $view_builder = $this->entityManager()->getViewBuilder('apache_openmeetings');

    return $view_builder->view($apache_openmeetings);
  }

  /**
   * Page title callback for a Apache Openmeetings  revision.
   *
   * @param int $apache_openmeetings_revision
   *   The Apache Openmeetings  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($apache_openmeetings_revision) {
    $apache_openmeetings = $this->entityManager()->getStorage('apache_openmeetings')->loadRevision($apache_openmeetings_revision);
    return $this->t('Revision of %title from %date', ['%title' => $apache_openmeetings->label(), '%date' => format_date($apache_openmeetings->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Apache Openmeetings .
   *
   * @param \Drupal\apache_openmeetings\Entity\ApacheOpenmeetingsInterface $apache_openmeetings
   *   A Apache Openmeetings  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(ApacheOpenmeetingsInterface $apache_openmeetings) {
    $account = $this->currentUser();
    $langcode = $apache_openmeetings->language()->getId();
    $langname = $apache_openmeetings->language()->getName();
    $languages = $apache_openmeetings->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $apache_openmeetings_storage = $this->entityManager()->getStorage('apache_openmeetings');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $apache_openmeetings->label()]) : $this->t('Revisions for %title', ['%title' => $apache_openmeetings->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all apache openmeetings revisions") || $account->hasPermission('administer apache openmeetings entities')));
    $delete_permission = (($account->hasPermission("delete all apache openmeetings revisions") || $account->hasPermission('administer apache openmeetings entities')));

    $rows = [];

    $vids = $apache_openmeetings_storage->revisionIds($apache_openmeetings);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\apache_openmeetings\ApacheOpenmeetingsInterface $revision */
      $revision = $apache_openmeetings_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $apache_openmeetings->getRevisionId()) {
          $link = $this->l($date, new Url('entity.apache_openmeetings.revision', ['apache_openmeetings' => $apache_openmeetings->id(), 'apache_openmeetings_revision' => $vid]));
        }
        else {
          $link = $apache_openmeetings->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.apache_openmeetings.translation_revert', ['apache_openmeetings' => $apache_openmeetings->id(), 'apache_openmeetings_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.apache_openmeetings.revision_revert', ['apache_openmeetings' => $apache_openmeetings->id(), 'apache_openmeetings_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.apache_openmeetings.revision_delete', ['apache_openmeetings' => $apache_openmeetings->id(), 'apache_openmeetings_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['apache_openmeetings_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
