<?php

namespace Drupal\apache_openmeetings\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Apache Openmeetings revision.
 *
 * @ingroup apache_openmeetings
 */
class ApacheOpenmeetingsRevisionDeleteForm extends ConfirmFormBase {


  /**
   * The Apache Openmeetings revision.
   *
   * @var \Drupal\apache_openmeetings\Entity\ApacheOpenmeetingsInterface
   */
  protected $revision;

  /**
   * The Apache Openmeetings storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $ApacheOpenmeetingsStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a new ApacheOpenmeetingsRevisionDeleteForm.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   *   The entity storage.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(EntityStorageInterface $entity_storage, Connection $connection) {
    $this->ApacheOpenmeetingsStorage = $entity_storage;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_manager = $container->get('entity.manager');
    return new static(
      $entity_manager->getStorage('apache_openmeetings'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'apache_openmeetings_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Are you sure you want to delete the revision from %revision-date?', ['%revision-date' => format_date($this->revision->getRevisionCreationTime())]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.apache_openmeetings.version_history', ['apache_openmeetings' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $apache_openmeetings_revision = NULL) {
    $this->revision = $this->ApacheOpenmeetingsStorage->loadRevision($apache_openmeetings_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->ApacheOpenmeetingsStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Apache Openmeetings: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    drupal_set_message(t('Revision from %revision-date of Apache Openmeetings %title has been deleted.', ['%revision-date' => format_date($this->revision->getRevisionCreationTime()), '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.apache_openmeetings.canonical',
       ['apache_openmeetings' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {apache_openmeetings_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.apache_openmeetings.version_history',
         ['apache_openmeetings' => $this->revision->id()]
      );
    }
  }

}
