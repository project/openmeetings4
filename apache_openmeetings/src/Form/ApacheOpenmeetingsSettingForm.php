<?php

namespace Drupal\apache_openmeetings\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ApacheOpenmeetingsSettingForm.
 */
class ApacheOpenmeetingsSettingForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'apache_openmeetings.apacheopenmeetingssetting',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'apache_openmeetings_setting_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('apache_openmeetings.apacheopenmeetingssetting');
    $form['apache_openmeetings_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Apache Openmeetings Username'),
      '#description' => $this->t('Apache Openmeetings Username'),
      '#maxlength' => 64,
      '#size' => 30,
      '#default_value' => $config->get('apache_openmeetings_username'),
    ];
    $form['apache_openmeetings_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Apache Openmeetings Password'),
      '#description' => $this->t('Apache Openmeetings Password'),
      '#maxlength' => 64,
      '#size' => 30,
      '#default_value' => $config->get('apache_openmeetings_password'),
    ];
    $form['apache_openmeetings_http_protocal'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Apache Openmeetings HTTP Protocal'),
      '#description' => $this->t('Apache Openmeetings HTTP Protocal'),
      '#maxlength' => 4,
      '#size' => 10,
      '#default_value' => $config->get('apache_openmeetings_http_protocal'),
    ];
    $form['apache_openmeetings_application'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Apache Openmeetings Application Name'),
      '#description' => $this->t('Apache Openmeetings Application Name'),
      '#maxlength' => 64,
      '#size' => 30,
      '#default_value' => $config->get('apache_openmeetings_application'),
    ];
    $form['apache_openmeetings_port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Apache Openmeetings Port'),
      '#description' => $this->t('Apache Openmeetings Port'),
      '#maxlength' => 64,
      '#size' => 10,
      '#default_value' => $config->get('apache_openmeetings_port'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('apache_openmeetings.apacheopenmeetingssetting')
      ->set('apache_openmeetings_username', $form_state->getValue('apache_openmeetings_username'))
      ->set('apache_openmeetings_password', $form_state->getValue('apache_openmeetings_password'))
      ->set('apache_openmeetings_http_protocal', $form_state->getValue('apache_openmeetings_http_protocal'))
      ->set('apache_openmeetings_application', $form_state->getValue('apache_openmeetings_application'))
      ->set('apache_openmeetings_port', $form_state->getValue('apache_openmeetings_port'))
      ->save();
  }

}
