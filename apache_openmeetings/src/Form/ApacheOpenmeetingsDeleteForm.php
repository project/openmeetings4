<?php

namespace Drupal\apache_openmeetings\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Apache Openmeetings entities.
 *
 * @ingroup apache_openmeetings
 */
class ApacheOpenmeetingsDeleteForm extends ContentEntityDeleteForm {


}
