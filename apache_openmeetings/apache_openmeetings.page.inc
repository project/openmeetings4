<?php

/**
 * @file
 * Contains apache_openmeetings.page.inc.
 *
 * Page callback for Apache Openmeetings entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Apache Openmeetings templates.
 *
 * Default template: apache_openmeetings.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_apache_openmeetings(array &$variables) {
  // Fetch ApacheOpenmeetings Entity Object.
  $apache_openmeetings = $variables['elements']['#apache_openmeetings'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
